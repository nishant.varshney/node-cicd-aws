var express    = require('express');        // call express
var app        = express();                 // define our app using express
var bodyParser = require('body-parser');


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 3000;        // set our port


var router = express.Router();              // get an instance of the express Router

router.get('/', function(req, res) {
    res.json({ message: 'welcome to the api! v2' });   
});


app.use('/api', router);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('port is listening at ' + port);